﻿
namespace CeasarChiper
{
    public interface IChipher
    {
        /// <summary>
        /// Encrypting given text by the given size of the key
        /// </summary>
        /// <param name="message">Message that needs to be encrypted</param>
        /// <param name="keyShift">Shift size for message encryption</param>
        /// <returns>Encrypted message</returns>
        abstract string EncrypMessage(string message, int keyShift);

        /// <summary>
        /// Decrypting given text by keyShift size
        /// </summary>
        /// <param name="message">Encrypted message that needs to be decrypted</param>
        /// <param name="keyShift">Shift size that was used for encrypting given message</param>
        /// <returns>Decrypted message</returns>
        abstract string DecryptMessage(string message, int keyShift);
    }
    public class CeasarCrypter : IChipher
    {
        private string _chipText = "abcdefghijklmnopqrstuvwxyz0123456789";

        public string EncrypMessage(string message, int keyShift)
        {
            if (message == null )
            {
                return message;
            }
            string result = "";

            foreach (char c in message)
            {
                result += replaceChar(c, keyShift);
            }

            return result;
        }

        public string DecryptMessage(string message, int keyShift)
        {
            return EncrypMessage(message, _chipText.Length - keyShift);
        }

        private char replaceChar(char ch, int keyShift)
        {
            if (!char.IsLetter(ch) && !char.IsDigit(ch))
            {
                return ch;
            }

            char find = char.ToLower(ch);
            int cryptIndex = (_chipText.IndexOf(find) + keyShift) % _chipText.Length;
            cryptIndex = cryptIndex < 0 ? cryptIndex + _chipText.Length : cryptIndex;
            char crypted = char.IsUpper(ch) ? char.ToUpper(_chipText[cryptIndex]) : _chipText[cryptIndex];
            return crypted;
        }
    }
}
