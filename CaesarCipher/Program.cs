﻿using System;
using System.Text.RegularExpressions;

namespace CeasarChiper
{
    class Program
    {
        static void Main(string[] args)
        {
            var self = new Program();
            self.Run();
        }

        private void Run()
        {
            Console.WriteLine("Welcome to 'Ceasar cipher'");

            var crypter = new CeasarCrypter();

            do
            {
                var message = getMessageToCrypt();
                if (!Regex.IsMatch(message, @"^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$"))
                {
                    Console.WriteLine("Sorry, cannot crypt this message. The ext should only contain numbers, words, and spaces.");
                    continue;
                }
                var shiftKey = getKeyShiftSize();
                var encryptedM = crypter.EncrypMessage(message, shiftKey);
                Console.WriteLine("Your encrypted message:");
                Console.WriteLine(encryptedM);
                var decryptedM = crypter.DecryptMessage(encryptedM, shiftKey);
                Console.WriteLine("Your decrypted message:");
                Console.WriteLine(decryptedM);
            } while (continueInput());

            Console.WriteLine("Program finished work. Thanks and good luck!");
        }

        private bool continueInput()
        {
            Console.WriteLine("Do you want to crypt another message? Y/N");
            var answ = Console.ReadLine();
            if (answ != "" && (answ.ToLower().Equals("y") || answ.ToLower().Equals("yes")))
            {
                return true;
            }
            return false;
        }

        private int getKeyShiftSize()
        {
            Console.WriteLine("What shift size to use:");
            while (true)
            {
                var answ = Console.ReadLine();
                int res;
                if (Int32.TryParse(answ, out res))
                {
                    return res;
                }
                Console.WriteLine("Invalid data. Try again and enter a valid number.");
            }
        }

        private string getMessageToCrypt()
        {
            Console.WriteLine("Add a message that should be crypted:");
            while (true)
            {
                var answ = Console.ReadLine();
                if (answ != "")
                {
                    return answ;
                }
                Console.WriteLine("Message cannot be empty. Please try again.");
            }
        }
    }
}
