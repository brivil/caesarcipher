﻿using NUnit.Framework;
using System;

namespace CeasarChiper.Tests
{
    [TestFixture]
    class CeasarCrypterTests
    {
        private CeasarCrypter _crypter;

        [SetUp]
        public void SetUp()
        {
            _crypter = new CeasarCrypter();
        }

        [TestCase("", 0, "")]
        [TestCase(null, 0, null)]
        [TestCase("", -1, "")]
        [TestCase("   .", -1, "   .")]
        [TestCase("", 1, "")]
        [TestCase("A", 1, "B")]
        [TestCase("a", 1, "b")]
        [TestCase("z", 1, "0")]
        [TestCase("Z", 1, "0")]
        [TestCase("0", -1, "z")]
        [TestCase("a", -1, "9")]
        [TestCase("B", -2, "9")]
        [TestCase("hello0", 1, "ifmmp1")]
        [TestCase("hello0", 10, "rovvya")]
        [TestCase("hello0.hello0.", 10, "rovvya.rovvya.")]
        [TestCase("azAZ09", 2, "c1C12b")]
        [TestCase("c1C12b", -2, "azAz09")]
        [TestCase("superSecretMessage09", 35, "rtodqRdbqdsLdrr9fdz8")]
        [TestCase("superSecretMessage09", 36, "superSecretMessage09")]
        public void IsEncryptionCorrect(string message, int shiftKeySize, string expectedResult)
        {
            // Get encrypted message
            var result = _crypter.EncrypMessage(message, shiftKeySize);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase("", 0, "")]
        [TestCase(null, 0, null)]
        [TestCase("", -1, "")]
        [TestCase("", 1, "")]
        [TestCase("   .", -1, "   .")]
        [TestCase("B", 1, "A")]
        [TestCase("b", 1, "a")]
        [TestCase("0", 1, "z")]
        [TestCase("z", -1, "0")]
        [TestCase("9", -1, "a")]
        [TestCase("9", -2, "b")]
        [TestCase("ifmmp1", 1, "hello0")]
        [TestCase("rovvya", 10, "hello0")]
        [TestCase("rovvya.rovvya.", 10, "hello0.hello0.")]
        [TestCase("c1C12b", 2, "azAz09")]
        [TestCase("azAz09", -2, "c1C12b")]
        [TestCase("rtodqRdbqdsLdrr9fdz8", 35, "superSecretMessage09")]
        [TestCase("superSecretMessage09", 36, "superSecretMessage09")]
        public void IsDecryptionCorrect(string message, int shiftKeySize, string expectedResult)
        {
            // Get edcrypted message
            var result = _crypter.DecryptMessage(message, shiftKeySize);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }
    }
}
